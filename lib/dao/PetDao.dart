import 'package:doggis/models/ClienteModel.dart';
abstract class PetDao{

  void getPetsByID(ClienteModel clienteModel);
}