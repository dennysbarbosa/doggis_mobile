
abstract class ServicoDao{

  void getTodosServicos();
  void getServicosIds(DateTime data, List<String> ids);
}