import 'package:doggis/models/ClienteModel.dart';

abstract class ClienteDao{

  void getAllCliente();
  void updateClient(ClienteModel clienteModel);
  void removeClient(String id);
  void createClient(ClienteModel clienteModel);
  getClienteById(String id);
}