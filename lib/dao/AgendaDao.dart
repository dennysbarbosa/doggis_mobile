import 'package:doggis/models/AgendaModel.dart';
import 'package:doggis/models/ServicoModel.dart';
import 'package:doggis/models/UsuarioModel.dart';

abstract class AgendaDao{

  createAgenda(AgendaModel agendaModel, UsuarioModel usuarioModel);
  void  getAgendaDia(DateTime data);
  AgendaModel initAgendaModel(MapEntry<dynamic, dynamic> value);
}