import 'dart:async';

import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/controllers/CalendarController.dart';
import 'package:doggis/controllers/AgendamentoController.dart';
import 'package:doggis/dao/AgendaDao.dart';
import 'package:doggis/models/UsuarioModel.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:doggis/models/AgendaModel.dart';
import 'package:intl/intl.dart';
import 'package:doggis/dao/impl/ServicosDaoImpl.dart';

class AgendaDaoImpl implements AgendaDao {

  static final String _AGENDA = "agenda";
  static final String SUCCESS = "Agendamento realizado com sucesso.";
  static final String _FAIL = "Falha ao realizar o agendamento!";
  static final String _FIELD_EMPTY = "Informe todos os campos!";
  BaseController baseController;
  DatabaseReference _databaseReference = FirebaseDatabase.instance.reference();

  AgendaDaoImpl(BaseController baseController){
    this.baseController = baseController;
  }

  @override
  createAgenda(AgendaModel agendaModel, UsuarioModel usuarioModel) async {

    var value = await baseController.checkConexao();

    if(value) {

      if (agendaModel != null && agendaModel.getClienteID() != null &&
          agendaModel.getPetID() != null &&
          agendaModel.getTipoServicoId() != null &&
          agendaModel.getProfissionalId() != null &&
          agendaModel.getHorario() != null) {
        var map = {
          'clienteId': agendaModel.getClienteID(),
          'petId': agendaModel.getPetID(),
          'tipoServicoId': agendaModel.getTipoServicoId(),
          'profissionalId': agendaModel.getProfissionalId(),
          'horario': agendaModel.getHorario()
        };

        _databaseReference
            .child(_AGENDA)
            .child(agendaModel.getData())
            .push()
            .set(map)
            .then((r) {
          (baseController as AgendamentoController).processFinish(SUCCESS);
        }).catchError((onError) {
          (baseController as AgendamentoController).processFinish(_FAIL);
        });
      } else {
        (baseController as AgendamentoController).processFinish(_FIELD_EMPTY);
      }
    }else{
        (baseController as AgendamentoController).processFinish(null);
    }
  }

  @override
  Future getAgendaDia(DateTime data) async {

    var value = await baseController.checkConexao();

    if(value){
    List<String> listServicosIds = List();
    String formatterData = new DateFormat('dd-MM-yyyy').format(data);

    var db = _databaseReference.child(_AGENDA).child(formatterData);
        db.once().then((DataSnapshot snapshot) {
        print(snapshot.toString());
        Map<dynamic, dynamic> values = snapshot.value;

        if (values != null) {
          for (var entry in values.entries) {

            (baseController as CalendarController).listAgendaByDay.clear();
            (baseController as CalendarController).listAgendaByDay.add(initAgendaModel(entry));
            listServicosIds.add(entry.value["tipoServicoId"]);
          }
          ServicoDaoImpl(baseController).getServicosIds(data, listServicosIds);
        } else {
          (baseController as CalendarController).listAgendaByDay.clear();
          (baseController as CalendarController).processFinish(null);
        }
      });
    }else{
      (baseController as CalendarController).listAgendaByDay.clear();
      (baseController as CalendarController).processFinish(null);
    }
  }

  @override
  AgendaModel initAgendaModel(MapEntry<dynamic, dynamic> value){

      AgendaModel agendaModel = new AgendaModel();
      agendaModel.setId(value.key);
      agendaModel.setHorario(value.value["horario"]);
      agendaModel.setProfissionalId(value.value["profissionalId"]);

      return agendaModel;
  }


}
