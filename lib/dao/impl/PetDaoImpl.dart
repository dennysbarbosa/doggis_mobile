import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/controllers/SearchController.dart';
import 'package:doggis/dao/PetDao.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:doggis/models/PetModel.dart';
import 'package:firebase_database/firebase_database.dart';

class PetDaoImpl implements PetDao{
 
  static final String _PET = "pet";
  DatabaseReference _databaseReference = FirebaseDatabase.instance.reference();
  BaseController _baseController;

  PetDaoImpl(BaseController baseController){
    this._baseController = baseController;
  }
  @override
  void getPetsByID(ClienteModel clienteModel) {

    var db = _databaseReference.child(_PET).orderByChild("clienteId").equalTo(clienteModel.getId());
    db.once().then((DataSnapshot snapshot){

    List<PetModel> listPets = List();
     Map<dynamic, dynamic> values = snapshot.value;

       if(values == null){
         if (_baseController is SearchController) {
           (_baseController as SearchController).processFinish(false);
         }
       }else {
         for (var entry in values.entries) {
           if (entry.value["clienteId"] == clienteModel.getId()) {
             PetModel petModel = PetModel();
             petModel.setId(entry.key);
             petModel.setNome(entry.value["nome"]);
             petModel.setRaca(entry.value["raca"]);
             petModel.setPorte(entry.value["porte"]);
             petModel.setClienteID(entry.value["clienteId"]);
             petModel.setAlergia(entry.value["alergia"]);
             petModel.setDescricao(entry.value["descricao"]);
             petModel.setTipo(entry.value["tipo"]);
             listPets.add(petModel);
           }
         }
         clienteModel.setListPets(null);
         clienteModel.setListPets(listPets);
         MyApp.preLoadModel.setClienteModel(
             listPets.length > 0 ? clienteModel : null);

         if (_baseController is SearchController) {
           (_baseController as SearchController).processFinish(
               listPets.length > 0 ? true : false);
         }
       }
    });
  }

}