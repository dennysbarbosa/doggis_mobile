import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/controllers/CalendarController.dart';
import 'package:doggis/dao/ServicoDao.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:doggis/models/ServicoModel.dart';
import 'package:doggis/main.dart';

class ServicoDaoImpl implements ServicoDao{

  static final String _SERVICO = "servico";
  BaseController baseController;
  DatabaseReference _databaseReference = FirebaseDatabase.instance.reference();

  ServicoDaoImpl(BaseController baseController){
    this.baseController = baseController;
  }

  @override
  void getTodosServicos() {

    List<ServicoModel> listServicos = List();
    var db = _databaseReference.child(_SERVICO);
    db.once().then((DataSnapshot snapshot){

      Map<dynamic, dynamic> values = snapshot.value;

      for (var entry in values.entries) {
          listServicos.add(_configServicoModel(entry.key, entry.value));
      }
      MyApp.preLoadModel.setListServicos(listServicos);
      print("servicos carregados...");
    });
  }

  @override
  void getServicosIds(DateTime data, List<String> ids) {

    Map<DateTime, List> events;
    List<ServicoModel> listServicos = new List();
    for (int i = 0; i < ids.length; i++) {

      var db = _databaseReference.child(_SERVICO).child(ids[i]);
      db.once().then((DataSnapshot snapshot) {

        listServicos.add(_configServicoModel(snapshot.key, snapshot.value));
        if(i == ids.length - 1){

          events = {data.add(Duration(days: 0)): listServicos};
          (baseController as CalendarController).processFinish(events);
        }
      });
    }

  }

  ServicoModel _configServicoModel(String key, Map<dynamic, dynamic> value){

    ServicoModel servicoModel = ServicoModel();
    servicoModel.setId(key);
    servicoModel.setTipoServico(value["tipoServico"]);
    servicoModel.setTempoEstimado(value["tempoEstimado"]);
    servicoModel.setResponsabilidade(value["resposabilidade"]);
    servicoModel.setQtdPataz(value["qtdPataz"]);
    servicoModel.setQtdPontos(value["qtdPontos"]);
    servicoModel.setPreco(value["preco"]);

    return servicoModel;
  }

}