import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/controllers/LoginController.dart';
import 'package:doggis/dao/FuncionarioDao.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:doggis/models/UsuarioModel.dart';
import 'package:doggis/models/VeterinarioModel.dart';
import 'package:doggis/models/AtendenteModel.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:doggis/models/PetModel.dart';

class FuncionarioDaoImpl implements FuncionarioDao {

  static final String _FUNCIONARIO = "funcionario";
  DatabaseReference _databaseReference;
  BaseController baseController;

  FuncionarioDaoImpl(BaseController baseController){

    this.baseController = baseController;
    _databaseReference = FirebaseDatabase.instance.reference();
  }

   @override
   getFuncByID(String id)  {
      _databaseReference.child(_FUNCIONARIO).child(id).once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> value = snapshot.value;

      if(value != null) {

        UsuarioModel usuarioModel;
        if (value["cargo"] == "Atendente") {
          usuarioModel = new AtendenteModel();
        } else {
          usuarioModel = new VeterinarioModel();
        }
        usuarioModel.setId(snapshot.key);
        usuarioModel.setNome(value["nome"]);
        MyApp.preLoadModel.setUser(usuarioModel);
        (baseController as LoginController).processFinish(true);
      }
    });
  }

  @override
  void getFuncionarios() {

    _databaseReference.child(_FUNCIONARIO).once().then((DataSnapshot snapshot){

      if(snapshot != null && snapshot.value != null) {

        Map<dynamic, dynamic> values = snapshot.value;
        List<UsuarioModel> listUsuario = new List();
        for (var entry in values.entries) {
          if (entry.value["cargo"] == "Atendente") {
            AtendenteModel atendenteModel = new AtendenteModel();
            atendenteModel.setId(entry.key);
            atendenteModel.setNome(entry.value["nome"]);
            atendenteModel.setCPF(entry.value["CPF"]);
            atendenteModel.setIdentidade(entry.value["identidade"]);
            atendenteModel.setCargo(entry.value["cargo"]);
            atendenteModel.setDisponivel(entry.value["disponivel"]);

            listUsuario.add(atendenteModel);
          } else {
            VeterinarioModel veterinarioModel = new VeterinarioModel();
            veterinarioModel.setId(entry.key);
            veterinarioModel.setNome(entry.value["nome"]);
            veterinarioModel.setCPF(entry.value["CPF"]);
            veterinarioModel.setIdentidade(entry.value["identidade"]);
            veterinarioModel.setDisponivel(entry.value["disponivel"]);
            veterinarioModel.setRegistroCV(entry.value["registroCV"]);
            if(entry.value["tipoPet"] != null) {
              veterinarioModel.setList(entry.value["tipoPet"].cast<String>());
            }
            listUsuario.add(veterinarioModel);
          }
        }

        print("lista de funcionarios carregados...");
        MyApp.preLoadModel.setListFunc(listUsuario);
      }
    });

  }


  List<UsuarioModel> listByCargo(ClienteModel clienteModel){

    List<UsuarioModel> mainList = MyApp.preLoadModel.getListFunc();
    List<UsuarioModel> list = new List();
    list.addAll(MyApp.preLoadModel.getListFunc());

    for(PetModel petModel in clienteModel.getListPets()){

      String tipo = petModel.getTipo();

      for(int i = 0; i < mainList.length; i++) {

        bool valid = false;
        if((mainList[i] is VeterinarioModel)){

             List<String> listTipo = (mainList[i] as VeterinarioModel).getListTipos();

             for(String profissionalTipo in listTipo) {

               if (profissionalTipo != tipo){
                   if(!valid) {
                     list.remove(mainList[i]);
                   }
               }else{

                 valid = true;
                 if(!list.contains(mainList[i])) {

                   list.add(mainList[i]);
                 }
               }
             }

          }else{
             list.remove(mainList[i]);
          }
        }
    }
    return list;
    }


}