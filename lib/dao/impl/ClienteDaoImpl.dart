import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/controllers/ClienteController.dart';
import 'package:doggis/controllers/LoginController.dart';
import 'package:doggis/controllers/SearchController.dart';
import 'package:doggis/dao/ClienteDao.dart';
import 'package:doggis/dao/impl/PetDaoImpl.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:doggis/models/PetModel.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class ClienteDaoImpl implements ClienteDao {

  static final String _CLIENTE = "cliente";
  static final String SUCCESS = "Cliente incluído com sucesso.";
  static final String UPDATE_SUCCESS = "Cliente atualizado com sucesso.";
  static final String REMOVE_SUCCESS = "Cliente excluído com sucesso.";
  static final String _FAIL = "Aconteceu uma falha!";
  static final String _FIELD_EMPTY = "Informe todos os campos!";
  DatabaseReference _databaseReference = FirebaseDatabase.instance.reference();
  List<ClienteModel> listCliente = new List();
  State<StatefulWidget> state;
  BaseController baseController;

  ClienteDaoImpl(BaseController baseController) {
    this.baseController = baseController;
  }

  @override
  void getAllCliente() {
    List<ClienteModel> list;
    var db = FirebaseDatabase.instance.reference().child(_CLIENTE);
    db.once().then((DataSnapshot snapshot) {
      try {
        Map<dynamic, dynamic> values = snapshot.value;
        list = parseDataToListClienteModel(values);
        if (baseController is SearchController) {
          (baseController as SearchController).processFinish(list);
        }
        print("clientes carregados...");
        MyApp.preLoadModel.setListClientes(list);
      } catch (e) {}
    });
  }

  List<ClienteModel> parseDataToListClienteModel(Map<dynamic, dynamic> values) {
    List<ClienteModel> list = new List<ClienteModel>();
    for (var entry in values.entries) {
      ClienteModel clienteModel = new ClienteModel();
      clienteModel.setId(entry.key);
      clienteModel.setEmail(entry.value["email"]);
      clienteModel.setNome(entry.value["nome"]);
      clienteModel.setCPF(entry.value["CPF"]);
      clienteModel.setIdentidade(entry.value["identidade"]);
      clienteModel.setQtdPataz(entry.value["qtdPataz"]);
      clienteModel.setEndereco(entry.value["endereco"]);
      clienteModel.setPermitirFoto(entry.value["permitirFoto"]);

      if (entry.value["listPets"] != null) {
        List<PetModel> listPets = new List();
        for (var pet in entry.value["listPets"]) {
          PetModel petModel = new PetModel();
          petModel.setId(pet);
          listPets.add(petModel);
        }
        clienteModel.setListPets(listPets);
      }

      list.add(clienteModel);
    }

    return list;
  }

  @override
  getClienteById(String id) {

     _databaseReference.child(_CLIENTE).child(id).once().then((DataSnapshot snapshot) {

      Map<dynamic, dynamic> value = snapshot.value;
      print('chave encontrada:' + snapshot.key);
      ClienteModel clienteModel = new ClienteModel();
      clienteModel.setId(snapshot.key);
      clienteModel.setEmail(value["email"]);
      clienteModel.setNome(value["nome"]);
      clienteModel.setCPF(value["CPF"]);
      clienteModel.setIdentidade(value["identidade"]);
      clienteModel.setQtdPataz(value["qtdPataz"]);
      clienteModel.setEndereco(value["endereco"]);
      MyApp.preLoadModel.setUser(clienteModel);
      if(baseController != null) {
        (baseController as LoginController).processFinish(true);
      }
    });
  }

  @override
  void createClient(ClienteModel clienteModel) async {
    var value = await baseController.checkConexao();

    if (value) {

      if(clienteModel.getNome() != null && clienteModel.getIdentidade() != null &&
       clienteModel.getCPF() != null && clienteModel.getEmail() != null &&
       clienteModel.getEndereco() != null) {

        var map = {
          'nome' : clienteModel.getNome(),
          'CPF': clienteModel.getCPF(),
          'identidade' : clienteModel.getIdentidade(),
          'permitirFoto' : clienteModel.getPermitirFoto(),
          'email' : clienteModel.getEmail(),
          'qtdPataz' : clienteModel.getQtdPataz(),
          'endereco' : clienteModel.getEndereco(),
        };

        _databaseReference
            .child(_CLIENTE)
            .push()
            .set(map)
            .then((r) {
          (baseController as ClienteController).processFinish(SUCCESS);
        }).catchError((onError) {
          (baseController as ClienteController).processFinish(_FAIL);
        });
      }else{
        (baseController as ClienteController).processFinish(_FIELD_EMPTY);
      }
    }else{
      (baseController as ClienteController).processFinish(null);
    }
  }

  @override
  void updateClient(ClienteModel clienteModel) async {

    var value = await baseController.checkConexao();

    if (value) {

      if(clienteModel.getNome() != null && clienteModel.getIdentidade() != null &&
          clienteModel.getCPF() != null && clienteModel.getEmail() != null &&
          clienteModel.getEndereco() != null) {

        var map = {
          'nome' : clienteModel.getNome(),
          'CPF': clienteModel.getCPF(),
          'identidade' : clienteModel.getIdentidade(),
          'permitirFoto' : clienteModel.getPermitirFoto(),
          'email' : clienteModel.getEmail(),
          'qtdPataz' : clienteModel.getQtdPataz(),
          'endereco' : clienteModel.getEndereco(),
        };

        _databaseReference
            .child(_CLIENTE)
            .child(clienteModel.getId())
            .update(map)
            .then((r) {
          (baseController as ClienteController).processFinish(UPDATE_SUCCESS);
        }).catchError((onError) {
          (baseController as ClienteController).processFinish(_FAIL);
        });
      }else{
        (baseController as ClienteController).processFinish(_FIELD_EMPTY);
      }
    }else{
      (baseController as ClienteController).processFinish(null);
    }
  }

  @override
  void removeClient(String id) async {

    var value = await baseController.checkConexao();

    if (value) {

        _databaseReference
            .child(_CLIENTE)
            .child(id)
            .remove()
            .then((r) {
          (baseController as ClienteController).processFinish(REMOVE_SUCCESS);
        }).catchError((onError) {
          (baseController as ClienteController).processFinish(_FAIL);
        });
    }else{
      (baseController as ClienteController).processFinish(null);
    }
  }
}