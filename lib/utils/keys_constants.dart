
///
///Splash Screen
///
const String splashScreenKey = 'splashScreenKey';

const String loginScreenLoginFieldKey = 'loginScreenLoginFieldKey';
const String loginScreenPwdFieldKey = 'loginScreenPwdFieldKey';
const String loginScreenEnterButtonKey = 'loginScreenEnterButtonKey';
const String floatingActionButtonKey = 'floatingActionButtonKey';
const String dropDownButtonKey = 'dropDownButtonKey';
const String radioButtonKey = 'radioButtonKey';
const String iconButtonKey = 'iconButtonKey';
const String listViewKey = 'listViewKey';
