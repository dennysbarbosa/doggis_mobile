
import 'package:flutter/material.dart';
import 'package:doggis/views/SplashScreen.dart';
import 'package:doggis/views/Login.dart';
import 'package:doggis/views/Home.dart';
import 'package:doggis/views/Agendamento.dart';
import 'package:doggis/views/Search.dart';
import 'package:doggis/models/PreLoadModel.dart';
import 'package:doggis/views/Pataz.dart';
import 'package:flutter/services.dart';

void main() async {

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  static PreLoadModel preLoadModel = new PreLoadModel();
  static BuildContext genericContext;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Doggis',
      theme: ThemeData(
        primarySwatch: Colors.brown,
        fontFamily: 'Dosis_Bold'
      ),
      home: SplashScreen(),
      routes: <String, WidgetBuilder>{
        '/Login': (BuildContext context) =>Login(),
        '/Home': (BuildContext context) => Home(),
        '/Event': (BuildContext context) => Agendamento(),
        '/SearchList': (BuildContext context) => BuscarClientes(),
        '/Pataz' : (BuildContext context) => Pataz(),
        //'/Client' : (BuildContext context) => Client()
      },
    );
  }

}

