
import 'package:doggis/utils/Connected.dart';
import 'package:doggis/views/Generic.dart';

class BaseController {

  BaseController() {
   // checkConexao();
  }

   Future<bool> checkConexao() async {

    var value =  await Connected().validation();
    if(value != null && value){
      return value;
    }else if(!value){
      Generic.ackAlert();
      return value;
    }
  }
}