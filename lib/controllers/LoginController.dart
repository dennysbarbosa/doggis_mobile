import 'package:doggis/async_delegator_interface/AsyncDelegator.dart';
import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/dao/impl/ClienteDaoImpl.dart';
import 'package:doggis/dao/impl/FuncionarioDaoImpl.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginController extends BaseController implements AsyncDelegator{

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  static FirebaseUser user = null;

  singIn(String login, String password) async{

    user = await _firebaseAuth.signInWithEmailAndPassword(email: login, password: password);
    if(user != null){
      managerLogin();
    }
  }


  Future<String> getCurrentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user.uid;
  }

  Future<void> signOut() async {

    return _firebaseAuth.signOut();
  }

  void managerLogin(){

      if(user.email.split("@")[1].startsWith('doggis')) {
        FuncionarioDaoImpl(this).getFuncByID(user.uid);
      }else{
        ClienteDaoImpl(this).getClienteById(user.uid);
      }
  }

  @override
  void processFinish(object) {

    if (MyApp.preLoadModel.getUser() != null) {

      if (MyApp.preLoadModel.getUser() is ClienteModel) {
        Navigator.of(MyApp.genericContext).pushReplacementNamed('/Pataz');
      } else {
        Navigator.of(MyApp.genericContext).pushReplacementNamed('/Home');
      }
    }
  }

}