import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/dao/impl/ClienteDaoImpl.dart';
import 'package:doggis/dao/impl/PetDaoImpl.dart';
import 'package:doggis/async_delegator_interface/AsyncDelegator.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:flutter/foundation.dart';

class SearchController extends BaseController with ChangeNotifier implements AsyncDelegator{

  List<ClienteModel> listClientes;
  bool clienteCarregado;

  getAllClientes(){
    ClienteDaoImpl(this).getAllCliente();
  }

  getPetsByClientes(ClienteModel clienteModel){
    PetDaoImpl(this).getPetsByID(clienteModel);
  }

  @override
  void processFinish(object) {

    if(object is bool){
      clienteCarregado = object;
    }else {
      listClientes = object;
    }
    notifyListeners();
  }

}