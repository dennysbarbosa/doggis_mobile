import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/dao/impl/ClienteDaoImpl.dart';
import 'package:doggis/async_delegator_interface/AsyncDelegator.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:flutter/foundation.dart';

class ClienteController extends BaseController with ChangeNotifier implements AsyncDelegator{

  ClienteModel clienteModel;
  String response;

  ClienteController(){

    if(MyApp.preLoadModel.getUser() != null){
      if(MyApp.preLoadModel.getUser() is ClienteModel){
        clienteModel = MyApp.preLoadModel.getUser();
      }
    }
  }

  @override
  void processFinish(object) {

    response = object;
    notifyListeners();
  }

  createClient(String name, String cpf, String identidade, bool allowPicture,
      String email, String address){

    int qtdPatazInit = 10;

    ClienteModel clienteModel = ClienteModel();
    clienteModel.setNome(name);
    clienteModel.setCPF(cpf);
    clienteModel.setIdentidade(identidade);
    clienteModel.setEmail(email);
    clienteModel.setEndereco(address);
    clienteModel.setPermitirFoto(allowPicture);
    clienteModel.setQtdPataz(qtdPatazInit);

    ClienteDaoImpl(this).createClient(clienteModel);
  }

  updateClient(String name, String cpf, String identidade, bool allowPicture,
      String email, String address){

    int qtdPatazInit = 10;

    ClienteModel clienteModel = ClienteModel();
    clienteModel.setId(MyApp.preLoadModel.clienteModel.getId());
    clienteModel.setNome(name);
    clienteModel.setCPF(cpf);
    clienteModel.setIdentidade(identidade);
    clienteModel.setEmail(email);
    clienteModel.setEndereco(address);
    clienteModel.setPermitirFoto(allowPicture);
    clienteModel.setQtdPataz(qtdPatazInit);

    ClienteDaoImpl(this).updateClient(clienteModel);
  }

  removeClient(String id){
    ClienteDaoImpl(this).removeClient(id);
  }
}