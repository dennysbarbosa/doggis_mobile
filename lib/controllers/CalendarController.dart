import 'package:doggis/async_delegator_interface/AsyncDelegator.dart';
import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/dao/impl/AgendaDaoImpl.dart';
import 'package:doggis/models/AgendaModel.dart';
import 'package:flutter/foundation.dart';

class CalendarController extends BaseController with ChangeNotifier implements AsyncDelegator{

  Map<DateTime, List> listServicos;
  bool showProgress;
  List<AgendaModel> listAgendaByDay;

  CalendarController(){
    showProgress = true;
    listAgendaByDay = new List();
  }

  getAgenda(DateTime day){
    AgendaDaoImpl(this).getAgendaDia(day);
  }

  @override
  void processFinish(object) {
    showProgress = false;
    listServicos = object;
    notifyListeners();
  }

}