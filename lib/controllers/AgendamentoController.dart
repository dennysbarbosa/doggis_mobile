import 'package:doggis/controllers/BaseController.dart';
import 'package:doggis/dao/impl/AgendaDaoImpl.dart';
import 'package:doggis/dao/impl/FuncionarioDaoImpl.dart';
import 'package:doggis/async_delegator_interface/AsyncDelegator.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/AgendaModel.dart';
import 'package:doggis/models/AtendenteModel.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:doggis/models/ItemDropModel.dart';
import 'package:doggis/models/ServicoModel.dart';
import 'package:doggis/models/UsuarioModel.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

class AgendamentoController extends BaseController with ChangeNotifier implements AsyncDelegator{

  ClienteModel _clienteModel;
  String funcIdSelecionado;
  String servicoIdSelecionado;
  String petIdSelecionado;
  List<UsuarioModel> listFunc;
  UsuarioModel usuarioModel;
  ServicoModel servicoModel;
  int typeFunc;
  AgendaDaoImpl _agendaDaoImpl;
  String response;
  String timeService;

  AgendamentoController(){
    _agendaDaoImpl = AgendaDaoImpl(this);
  }

  void initClienteModel(){
    _clienteModel = MyApp.preLoadModel.getClienteModel();
  }

  getClienteModel(){
    return _clienteModel;
  }

  void criarAgendamento(DateTime selectedDay, String horarioAgendamento, String clienteId){

    AgendaModel agendaModel = new AgendaModel();
    String formatterData = new DateFormat('dd-MM-yyyy').format(selectedDay);
    agendaModel.setData(formatterData);
    agendaModel.setHorario(horarioAgendamento);
    agendaModel.setClienteID(clienteId);
    agendaModel.setPetID(petIdSelecionado);
    agendaModel.setProfissionalId(funcIdSelecionado);
    agendaModel.setTipoServicoId(servicoIdSelecionado);
    _agendaDaoImpl.createAgenda(agendaModel, usuarioModel);
  }

  @override
  void processFinish(object) {

    response = object;
    notifyListeners();
  }

  ItemDropModel initValueDrop(object){

    ItemDropModel itemDropModel = ItemDropModel();
    itemDropModel.setId(object.getId());
    itemDropModel.setValue(object is ServicoModel ? object.getTipoServico() : object.getNome());

    return itemDropModel;
  }

  void validarTypeFunc(){
      usuarioModel is AtendenteModel ? typeFunc = 2 : typeFunc = 1;
  }

  List<UsuarioModel> listTypeEmployee(){
    return FuncionarioDaoImpl(null).listByCargo(_clienteModel);
  }

  List<ServicoModel> listServices(){
    return MyApp.preLoadModel.listServicos;
  }

  loadUserSelected(){
    this.usuarioModel = null;
    for(UsuarioModel usuarioModel in MyApp.preLoadModel.getListFunc()){

      if(usuarioModel.getId() == funcIdSelecionado){

        this.usuarioModel = usuarioModel;
        validarTypeFunc();
        break;
      }
    }
  }

  loadServiceSelected(){

    for(ServicoModel servicoModel in MyApp.preLoadModel.listServicos){

      if(servicoModel.getId() == servicoIdSelecionado){
        this.servicoModel = servicoModel;
      }
    }
  }

  // valida se o funcionario está com horário disponível
  bool validarHourFunc(DateTime currentTime, List<AgendaModel> list){

    DateFormat dateFormat = new DateFormat("H:m");
    bool value = true;
    int serviceTime = int.parse(servicoModel.getTempoEstimado());
    DateTime time = currentTime.add(Duration(minutes: serviceTime));
    String minute = time.minute < 10 ? "0" + time.minute.toString() : time.minute.toString();
    timeService = time.hour.toString() + ":" + minute;



    if(list != null){
      for(int i = 0; i < list.length; i++) {

        DateTime init =  dateFormat.parse(list[i].getHorario());
        DateTime end = init.add(Duration(minutes: 30));

        if(currentTime.isAtSameMomentAs(init)){
          value = false;
          break;
        }else if (currentTime.isAfter(init) && currentTime.isBefore(end) && usuarioModel != null) {
          value = false;
          break;
        } else {
          value = true;
        }
      }
    }

    return value;
  }

}