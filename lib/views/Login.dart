import 'dart:async';

import 'package:doggis/controllers/LoginController.dart';
import 'package:doggis/main.dart';
import 'package:doggis/utils/field_validators.dart';
import 'package:doggis/utils/keys_constants.dart';
import 'package:doggis/views/animations/loginAnimation.dart';
import 'package:doggis/views/components/button.dart';
import 'package:doggis/views/components/text_field.dart';
import 'package:doggis/views/Generic.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter/services.dart';

class Login extends StatefulWidget {
  @override
  LoginState createState() {
    return new LoginState();
  }
}

class LoginState extends State<Login> with TickerProviderStateMixin, Generic {

  AnimationController _loginButtonController;
  var animationStatus = 0;
  FocusNode loginFieldFocus = new FocusNode();
  FocusNode pwdFieldFocus = new FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  LoginController loginController;
  String _login;
  String _password;

  @override
  void initState() {
    super.initState();
    loginController = new LoginController();
    _loginButtonController = new AnimationController(
        duration: new Duration(milliseconds: 5000), vsync: this);
  }

  @override
  void dispose() {
    _loginButtonController.dispose();
    loginFieldFocus.dispose();
    pwdFieldFocus.dispose();
    super.dispose();
  }

  Future<Null> _playAnimation() async {
    try {

      await _loginButtonController.forward();
      await _loginButtonController.reverse();
    } on TickerCanceled {}
  }

  @override
  Widget build(BuildContext context) {

    MyApp.genericContext = context;
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top, SystemUiOverlay.bottom]);
    timeDilation = 0.6;
    return (new WillPopScope(
        child: new Scaffold(
      key: scaffoldKey,
      body:Form(
      key: _formKey,
      child: new Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/images/degrade.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: new Container(
              child: new ListView(
            padding: const EdgeInsets.all(0.0),
            children: <Widget>[
              new Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[

                      Padding(
                          padding: const EdgeInsets.only(
                            left: 0,
                            top: 60,
                            right: 0,
                            bottom: 0,
                          ),
                          child: Container(
                            child: new Image.asset("assets/images/logo.png", width: 60, height: 60))),
                      Padding(
                          padding: const EdgeInsets.only(
                            left: 0,
                            top: 0,
                            right: 0,
                            bottom: 0,
                          ),
                          child: Container(
                              child: Text("Doggis",  style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Dosis_Bold', fontSize: 50, color: Colors.brown)))),
                      Padding(
                          padding: const EdgeInsets.only(
                            left: 20,
                            top: 10,
                            right: 20,
                            bottom: 0,
                          ),
                          child: Container(
                              child: new Align(
                                  alignment: Alignment.topCenter,
                                  child: new TextFieldApp(
                                    key: Key(loginScreenLoginFieldKey),
                                    type: TextFieldApp
                                        .TYPE_TXT_FIELD_INPUT_DECORATION,
                                    text: "Login",
                                    parentContext: context,
                                    fieldFocusId: loginFieldFocus,
                                    nextFieldFocusId: pwdFieldFocus,
                                    saveFieldCallbackFunction:
                                    saveLoginFieldCallbackFunction,
                                  )))),
                      Padding(
                          padding: const EdgeInsets.only(
                            left: 20,
                            top: 10,
                            right: 20,
                            bottom: 100,
                          ),
                          child: Container(
                              child: new Align(
                                  alignment: Alignment.topCenter,
                                  child: new TextFieldApp(
                                    key: Key(loginScreenPwdFieldKey),
                                    type: TextFieldApp
                                        .TYPE_TXT_FIELD_INPUT_DECORATION,
                                    text: "Senha",
                                    fieldFocusId: pwdFieldFocus,
                                    saveFieldCallbackFunction:
                                    savePasswordFieldCallbackFunction,
                                    callbackFunction: () =>
                                        validateFormInputs(
                                            "")
                                  )))),

                    ],
                  ),
                  animationStatus == 0
                      ? new Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: new InkWell(
                             onTap: (){
                                  validateFormInputs("");
                             },
                              child: new ButtonApp(
                                  key: Key(loginScreenEnterButtonKey),
                                  type: ButtonApp.TYPE_COMMON_BUTTON,
                                  text: "Entrar")),
                        )
                      : new StaggerAnimation(
                          buttonController: _loginButtonController.view, loginController: loginController),
                ],
              ),
            ],
          )))),
    )));
  }

  void validateFormInputs(String invalidPassword) {

    if (_formKey.currentState.validate()) {

      _formKey.currentState.save();
      loginController.singIn(_login, _password);
      animationStatus = 1;
      setState(() {});
      _playAnimation();
    } else {

      String value = LoginFieldValidator.validate(_login, _password);
      if(value != null) {
        snackBar(scaffoldKey, "Campos, login e senha devem ser preenchidos.");
      }
    }
  }

  void saveLoginFieldCallbackFunction(String textInput) {
    _login = textInput;
  }

  void savePasswordFieldCallbackFunction(String textInput) {
    _password = textInput;
  }
}
