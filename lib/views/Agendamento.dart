import 'dart:async';

import 'package:doggis/controllers/AgendamentoController.dart';
import 'package:doggis/dao/impl/AgendaDaoImpl.dart';
import 'package:doggis/models/AgendaModel.dart';
import 'package:doggis/models/ItemDropModel.dart';
import 'package:doggis/models/ServicoModel.dart';
import 'package:doggis/models/UsuarioModel.dart';
import 'package:doggis/utils/keys_constants.dart';
import 'package:doggis/views/Search.dart';
import 'package:doggis/views/Generic.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class Agendamento extends StatefulWidget {
  DateTime selectedDay;
  List<AgendaModel> listScheduleModel;

  Agendamento(
      {Key,
      key,
      @required this.selectedDay,
      @required this.listScheduleModel})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new AgendamentoState(selectedDay, listScheduleModel);
  }
}

class AgendamentoState extends State<Agendamento> with Generic {

  DateTime selectedDay;
  Map<DateTime, List> events;
  List<AgendaModel> listScheduleModel;

  List<DropdownMenuItem<String>> _dropDownMenuItemsPets;
  List<DropdownMenuItem<String>> _dropDownMenuItemsServicos;
  List<DropdownMenuItem<String>> _dropDownMenuItemsFunc;
  String _horarioAgendamento;
  AgendamentoController scheduleController;

  AgendamentoState(this.selectedDay, this.listScheduleModel);

  List<DropdownMenuItem<String>> getDropDownMenuItems(object) {
    List<dynamic> list = object;
    List<DropdownMenuItem<String>> items = new List();
    for (int i = 0; i < list.length; i++) {
      ItemDropModel itemDropModel = scheduleController.initValueDrop(list[i]);
      items.add(new DropdownMenuItem(
          value: itemDropModel.getId().toString(),
          child: Text(itemDropModel.getValue().toString())));
    }
    return items;
  }

  void changedDropDownItemFunc(String funcIdSeleciondo) {
    setState(() {

      scheduleController.funcIdSelecionado = funcIdSeleciondo;
      _horarioAgendamento = null;
      scheduleController.loadUserSelected();
    });
  }

  void changedDropDownItemServicos(String servicoIdSeleciondo) {
    setState(() {
      scheduleController.servicoIdSelecionado = servicoIdSeleciondo;
      scheduleController.loadServiceSelected();
    });
  }

  void changedDropDownItemPets(String petIdSelecionado) {
    setState(() {
      scheduleController.petIdSelecionado = petIdSelecionado;
    });
  }

  void goList() async {
    var result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => BuscarClientes(
              contextEvent: context,
            )));
    if (result == null) {

      //setState(() {
      scheduleController.initClienteModel();

      if (scheduleController.getClienteModel() != null) {
        List<ServicoModel> listServicos = scheduleController.listServices();
        if (listServicos != null) {
          _dropDownMenuItemsServicos = getDropDownMenuItems(listServicos);
          scheduleController.servicoIdSelecionado =
              _dropDownMenuItemsServicos[0].value;
          scheduleController.loadServiceSelected();
        }

        _dropDownMenuItemsPets = getDropDownMenuItems(
            scheduleController.getClienteModel().getListPets());
        scheduleController.petIdSelecionado = _dropDownMenuItemsPets[0].value;
        List<UsuarioModel> listFunc = scheduleController.listTypeEmployee();

        if (listFunc != null) {
          _dropDownMenuItemsFunc =
              getDropDownMenuItems(scheduleController.listTypeEmployee());
          scheduleController.funcIdSelecionado =
              _dropDownMenuItemsFunc[0].value;
          scheduleController.loadUserSelected();
        }
      }

      // });
    }
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        builder: (context) => scheduleController = AgendamentoController(),
        child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.yellow[300],
            appBar: AppBar(
              title: Text("Data do Agendamento: " + selectedDay.day.toString() + "/"+ selectedDay.month.toString() + "/" + selectedDay.year.toString(), style: TextStyle(fontSize: 15),),
            ),
            body: Container(
                width: double.infinity,
                margin: EdgeInsets.only(right: 8, left: 8, top: 20),
                child: new Stack(
                    children: <Widget>[
                      Positioned(
                          child: Column(children: <Widget>[
                        new Row(children: <Widget>[
                          Text("Consultar Cliente ",
                              style: TextStyle(fontSize: 18)),

                              IconButton(
                                key: Key(iconButtonKey),
                                iconSize: 30.0,
                                icon: Icon(Icons.search),
                                onPressed: () {
                                  goList();
                                },
                              ),
                        ]),
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            width: double.infinity,
                            child: scheduleController != null &&
                                    scheduleController.getClienteModel() != null
                                ? new Row(children: <Widget>[
                                    Expanded(
                                      child: new Text("Nome do cliente - ",
                                          style: TextStyle(fontSize: 18)),
                                      flex: 2,
                                    ),
                                    Expanded(
                                      child: new Text(
                                          scheduleController
                                              .getClienteModel()
                                              .getNome(),
                                          style: TextStyle(fontSize: 16)),
                                      flex: 3,
                                    ),
                                  ])
                                : Container()),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: scheduleController != null &&
                                  scheduleController.getClienteModel() != null
                              ? new Row(children: <Widget>[
                                  Expanded(
                                    child: new Text("Pet: ",
                                        style: TextStyle(fontSize: 18)),
                                    flex: 2,
                                  ),
                                  Expanded(
                                      flex: 3,
                                      child: new Align(
                                        alignment: AlignmentDirectional.center,
                                        child: scheduleController != null &&
                                                scheduleController
                                                        .getClienteModel() ==
                                                    null
                                            ? Container()
                                            : new DropdownButton(
                                                isExpanded: true,
                                                value: scheduleController
                                                    .petIdSelecionado,
                                                items: _dropDownMenuItemsPets,
                                                onChanged:
                                                    changedDropDownItemPets,
                                              ),
                                      ))
                                ])
                              : Container(),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: scheduleController != null &&
                              scheduleController.getClienteModel() != null
                            ? new Row(children: <Widget>[
                            Expanded(
                              child: new Text("Tipo de Serviço: ",
                                  style: TextStyle(fontSize: 18)),
                              flex: 2,
                            ),
                            Expanded(
                                flex: 3,
                                child: new Align(
                                    alignment: AlignmentDirectional.center,
                                    child:new DropdownButton(
                                            isExpanded: true,
                                            value: scheduleController
                                                .servicoIdSelecionado,
                                            items: _dropDownMenuItemsServicos,
                                            onChanged:
                                                changedDropDownItemServicos,
                                    )))
                          ]) :  Container(),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: scheduleController != null &&
                                  scheduleController.getClienteModel() != null
                              ? new Row(children: <Widget>[
                                  Expanded(
                                    child: new Text("Profissional: ",
                                        style: TextStyle(fontSize: 18)),
                                    flex: 2,
                                  ),
                                  Expanded(
                                      flex: 3,
                                      child: new Align(
                                        alignment: AlignmentDirectional.center,
                                        child: new DropdownButton(
                                          isExpanded: true,
                                          value: scheduleController
                                              .funcIdSelecionado,
                                          items: _dropDownMenuItemsFunc,
                                          onChanged: changedDropDownItemFunc,
                                        ),
                                      ))
                                ])
                              : Container(),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          child: scheduleController != null &&
                                  scheduleController.getClienteModel() != null
                              ? new Row(children: <Widget>[
                                  Radio(
                                    key: Key(radioButtonKey),
                                    groupValue: scheduleController.typeFunc,
                                    //onChanged: changeRadio,
                                    value: 1,
                                  ),
                                  Text(
                                    'Veterinário',
                                    style: new TextStyle(fontSize: 18.0),
                                  ),
                                  Radio(
                                    groupValue: scheduleController.typeFunc,
                                    //onChanged: changeRadio,
                                    value: 2,
                                  ),
                                  Text(
                                    'Atendente',
                                    style: new TextStyle(fontSize: 18.0),
                                  ),
                                ])
                              : Container(),
                        ),

                        ContainerTime(this),
                        Container(
                            margin: EdgeInsets.only(top: 20),
                            width: double.infinity,
                            child: _horarioAgendamento != null
                                ? new Row(children: <Widget>[
                              Expanded(
                                child: new Text("Tempo estimado:",
                                    style: TextStyle(fontSize: 18)),
                                flex: 2,
                              ),
                              Expanded(
                                child: new Text(
                                    scheduleController
                                        .timeService,
                                    style: TextStyle(fontSize: 18)),
                                flex: 3,
                              ),
                            ])
                                : Container()),
                      ])),

                      new Positioned(child: ButtonCreate(this))
                    ]))));
  }
}

class ContainerTime extends StatelessWidget {
  AgendamentoState _eventState;

  ContainerTime(this._eventState);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10),
        child: _eventState.scheduleController != null &&
                _eventState.scheduleController.getClienteModel() != null
            ? new Row(children: <Widget>[
                new Text(
                    _eventState._horarioAgendamento == null
                        ? "Horário do atendimento"
                        : "Horário agendado para ",
                    style: TextStyle(fontSize: 18)),
                Container(
                  child: _eventState._horarioAgendamento == null
                      ? IconButton(
                          iconSize: 30.0,
                          icon: Icon(Icons.watch),
                          onPressed: () {
                            selectTime(context);
                          },
                        )
                      : new Text(_eventState._horarioAgendamento,
                          style: TextStyle(fontSize: 18)),
                ),
              ])
            : Container());
  }

  Future<Null> selectTime(BuildContext context) async {

    DateFormat dateFormat = new DateFormat("H:mm");
    DateTime init = dateFormat.parse("09:00");
    DateTime end = dateFormat.parse("19:01");
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
          child: child,
        );
      },
    );

    if (picked != null) {

      String invalidTime = "Horário Inválido! informe entre 9:00 e 19:00.";
      DateTime currentTime = dateFormat.parse(
          picked.hour.toString() + ":" + picked.minute.toString());

      DateTime timeSystem = dateFormat.parse(
          DateTime.now().hour.toString() + ":" + DateTime.now().minute.toString());

      if (currentTime.isBefore(timeSystem) && !_eventState.selectedDay.isAfter(DateTime.now())) {
        _eventState.snackBar(_eventState._scaffoldKey,
            invalidTime);
      } else {
        if (currentTime.isAfter(init) && currentTime.isBefore(end)) {
          bool disponivel = _eventState.scheduleController.validarHourFunc(
              currentTime, _eventState.listScheduleModel);

          if (disponivel) {
            _eventState.setState(() {
              String minute = picked.minute < 10 ? "0" +
                  picked.minute.toString() : picked.minute.toString();
              _eventState._horarioAgendamento =
                  picked.hour.toString() + ":" + minute;
            });
          } else {
            _eventState.snackBar(_eventState._scaffoldKey,
                "O profissional possui agendamento para o horário informado.");
          }
        } else {
          _eventState.snackBar(_eventState._scaffoldKey,
              invalidTime);
        }
      }
    }
  }

}

class ButtonCreate extends StatelessWidget {
  AgendamentoState _eventState;
  BuildContext buildContext;

  ButtonCreate(AgendamentoState event) {
    this._eventState = event;
  }

  @override
  Widget build(BuildContext context) {
    AgendamentoController scheduleController =
        Provider.of<AgendamentoController>(context);

    if (scheduleController.response != null) {
      _eventState.snackBar(
          _eventState._scaffoldKey, scheduleController.response);

      if(scheduleController.response == AgendaDaoImpl.SUCCESS){
        Generic.updateEvents = true;
        scheduleController.response = null;
        backHome();
      }
      scheduleController.response = null;
    }

    return new Align(
      alignment: FractionalOffset.bottomCenter,
      child: GestureDetector(
          onTap: () {
            criarAgenda(scheduleController);
          },
          child: Container(
            margin: EdgeInsets.only(bottom: 50),
            width: 420.0,
            height: 45.0,
            alignment: FractionalOffset.center,
            decoration: new BoxDecoration(
                color: Colors.brown,
                borderRadius: new BorderRadius.all(const Radius.circular(5.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors /*.black,*/ .grey[600],
                    blurRadius: 6.0,
                    // has the effect of softening the shadow
                    //spreadRadius: 1.0, // has the effect of extending the shadow
                    offset: Offset(0.0, 2.0),
                  )
                ]),
            child: new Text(
              "SALVAR",
              style: new TextStyle(
                color: Colors.white,
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.3,
              ),
            ),
          )),
    );
  }

  criarAgenda(AgendamentoController scheduleController) {

    if (_eventState.scheduleController.getClienteModel() != null) {
      scheduleController.criarAgendamento(
          _eventState.selectedDay,
          _eventState._horarioAgendamento,
          _eventState.scheduleController.getClienteModel().getId());
    } else {
      _eventState.snackBar(_eventState._scaffoldKey, "informe o cliente!");
    }
  }

  backHome(){

    int time =  1000;
    Future.delayed(Duration(milliseconds: time), () {

      Navigator.pop(MyApp.genericContext);
    });
  }
}
