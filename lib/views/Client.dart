import 'package:doggis/controllers/ClienteController.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:doggis/views/Generic.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Client extends StatefulWidget{

  String type;
  Client(this.type);

  @override
  State<StatefulWidget> createState() {
    return new ClienteState(type);
  }
}

class ClienteState extends State<Client> with Generic{

  String type;
  ClienteModel _cLienteModel;
  static bool _allow;

  ClienteState(this.type){

    ClienteState._allow = true;
    if(type != "Incluir"){

      _cLienteModel = MyApp.preLoadModel.getClienteModel();
      nameController.text = _cLienteModel.getNome();
      identidadeController.text = _cLienteModel.getIdentidade();
      cpfController.text = _cLienteModel.getCPF();
      emailController.text = _cLienteModel.getEmail();
      addressController.text = _cLienteModel.getEndereco();
      radioButtonValue =  _cLienteModel.getPermitirFoto() ? 1 : 2;
    }
   }

  int radioButtonValue = 1;
  bool allowPicture = true;
  FocusNode textSecondFocusNode = new FocusNode();
  FocusNode textThirdFocusNode = new FocusNode();
  FocusNode textFourthFocusNode = new FocusNode();
  FocusNode textFifthFocusNode = new FocusNode();
  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController identidadeController = new TextEditingController();
  TextEditingController cpfController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ClienteController clienteController;

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
      builder: (context) => clienteController = ClienteController(),
      child: WillPopScope(
        onWillPop: () {
          return Future.value(_allow); // if true allow back else block it
        },
        child: Scaffold(
        backgroundColor: Colors.yellow[300],
        key: _scaffoldKey,
          appBar: AppBar(
              title: Text(type +" Cliente")),
          body: SingleChildScrollView(

              child: Stack(
                  children: <Widget>[

                    Positioned(
                      child: Column(children: <Widget>[
                        Container(
                            margin: EdgeInsets.only(top: 10, right: 10),
                            height: 40,
                            width: double.infinity,
                            child: new Row(children: <Widget>[
                              Expanded(
                                child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 20, right: 10),
                                    child: new Text("Nome :",
                                        style: TextStyle(fontSize: 18))),
                                flex: 1,
                              ),
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: new TextFormField(
                                    enabled: type == "Excluir" || type == "Buscar" ? false :  true,
                                    controller: nameController,
                                      textInputAction: TextInputAction.next,
                                      onFieldSubmitted: (String value) {
                                        FocusScope.of(context).requestFocus(textSecondFocusNode);
                                      },
                                      style: TextStyle(fontSize: 18)),

                                ),flex: 3,)
                            ])
                        ),

                        Container(
                            height: 40,
                            margin: EdgeInsets.only(top: 30, left: 10, right: 10),
                            //margin: EdgeInsets.only(top: 10),
                            width: double.infinity,
                            child: new Row(children: <Widget>[
                              Expanded(
                                child: Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    child: new Text("Identidade :",
                                        style: TextStyle(fontSize: 18))),
                                flex: 1,
                              ),
                              Expanded(
                                child: Container(

                                  child: new TextFormField(
                                      enabled: type == "Excluir" || type == "Buscar" ? false :  true,
                                      maxLength: 11,
                                      keyboardType: TextInputType.number,
                                      controller: identidadeController,
                                      focusNode: textSecondFocusNode,
                                      textInputAction: TextInputAction.next,
                                      onFieldSubmitted: (String value) {
                                        FocusScope.of(context).requestFocus(textThirdFocusNode);
                                      },
                                      style: TextStyle(fontSize: 18)),

                                ),flex: 3,)
                            ])
                        ),
                        Container(
                            height: 40,
                            margin: EdgeInsets.only(top: 20, left: 10, right: 10),
                            //margin: EdgeInsets.only(top: 10),
                            width: double.infinity,
                            child: new Row(children: <Widget>[
                              Expanded(
                                child: Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    child: new Text("CPF :",
                                        style: TextStyle(fontSize: 18))),
                                flex: 1,
                              ),
                              Expanded(
                                child: Container(

                                  child: new TextFormField(
                                      enabled: type == "Excluir" || type == "Buscar" ? false :  true,
                                      maxLength: 11,
                                      keyboardType: TextInputType.number,
                                      controller: cpfController,
                                      focusNode: textThirdFocusNode,
                                      textInputAction: TextInputAction.next,
                                      onFieldSubmitted: (String value) {
                                        FocusScope.of(context).requestFocus(textFourthFocusNode);
                                      },
                                      style: TextStyle(fontSize: 18)),

                                ),flex: 3,),

                            ])
                        ),
                        Container(
                            height: 40,
                            margin: EdgeInsets.only(right: 10),
                            //margin: EdgeInsets.only(top: 10),
                            width: double.infinity,
                            child: new Row(children: <Widget>[
                              Expanded(
                                child: Container(
                                    margin: EdgeInsets.only(left: 10, top: 20, right: 10),
                                    child: new Text("Email :",
                                        style: TextStyle(fontSize: 18))),
                                flex: 1,
                              ),
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: new TextFormField(
                                      enabled: type == "Excluir" || type == "Buscar" ? false :  true,
                                      controller: emailController,
                                      focusNode: textFourthFocusNode,
                                      textInputAction: TextInputAction.next,
                                      onFieldSubmitted: (String value) {
                                        FocusScope.of(context).requestFocus(textFifthFocusNode);
                                      },
                                      style: TextStyle(fontSize: 18)),

                                ),flex: 3,)
                            ])
                        ),
                        Container(

                            margin: EdgeInsets.all(10),
                            //margin: EdgeInsets.only(top: 10),
                            width: double.infinity,
                            child: new Row(children: <Widget>[
                              Expanded(
                                child: Container(
                                    child: new Text("Endereço :",
                                        style: TextStyle(fontSize: 18))),
                                flex: 1,
                              ),
                              Expanded(
                                child: Container(
                                  child: new TextFormField(
                                      enabled: type == "Excluir" || type == "Buscar" ? false :  true,
                                      controller: addressController,
                                      focusNode: textFifthFocusNode,
                                      keyboardType: TextInputType.multiline,
                                      maxLines: 2
                                      ),

                                ),flex: 3,)
                            ])
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: new Row(children: <Widget>[
                            Radio(
                              groupValue: radioButtonValue,
                              onChanged: changeRadio,
                              value: 1,
                            ),
                            Text(
                              'Permitir foto',
                              style: new TextStyle(fontSize: 18.0),
                            ),
                            Radio(
                              groupValue: radioButtonValue,
                              onChanged: changeRadio,
                              value: 2,
                            ),
                            Text(
                              'Não permitir foto',
                              style: new TextStyle(fontSize: 18.0),
                            ),
                          ])

                        ),

                        Container(
                            margin: EdgeInsets.only(left: 10, top: 10),
                            child: Row(children: <Widget>[
                          Text("Consultar ou adicionar Pet ",
                              style: TextStyle(fontSize: 18)),
                          Expanded(
                              flex: 2,
                              child: IconButton(
                                iconSize: 30.0,
                                icon: Icon(Icons.search, color: Colors.brown,),
                                onPressed: () {
                                  showSnackbar();
                                },
                              )),
                          Expanded(
                              flex: 2,
                              child: IconButton(
                                  iconSize: 30.0,
                                  icon: Icon(Icons.add, color: Colors.brown),
                                  onPressed: () {
                                    showSnackbar();
                                  }))
                        ])),

                        ButtonCreate(this),

                      ],
                      ),
                    ),
                  ]
              )
          )
        ),
      )
    );


  }

  void showSnackbar(){
    snackBar(_scaffoldKey, 'Caso de Uso não implementado ;)');
  }

  changeRadio(int i) {
    setState(() {
     radioButtonValue = i;
     i == 1 ? allowPicture = true : allowPicture = false;
    });
  }
}

class ButtonCreate extends StatelessWidget {

  ClienteState _clientState;
//  BuildContext buildContext;

  ButtonCreate(ClienteState clientState) {
    this._clientState = clientState;
  }

  @override
  Widget build(BuildContext context) {
    ClienteController clienteController =
    Provider.of<ClienteController>(context);

    if (clienteController.response != null) {
      _clientState.snackBar(
          _clientState._scaffoldKey, clienteController.response);
      backHome();
    }

    return new Align(
      alignment: FractionalOffset.bottomCenter,
      child: GestureDetector(
          onTap: () {

              if(_clientState.type == "Incluir" || _clientState.type == "Alterar"){

             print(_clientState.cpfController.text);
             if(_clientState.nameController.text.isEmpty ||
                _clientState.cpfController.text.isEmpty ||
                _clientState.identidadeController.text.isEmpty ||
                _clientState.emailController.text.isEmpty||
                _clientState.addressController.text.isEmpty) {

               _clientState.snackBar(
                   _clientState._scaffoldKey, "Todos os campos são obrigatórios");
                }else{
                  managerClient();
               }
              }else{
                  managerClient();
              }
            },
          child: Container(
            margin: EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 10),
            width: 420.0,
            height: 45.0,
            alignment: FractionalOffset.center,
            decoration: new BoxDecoration(
                color: Colors.brown,
                borderRadius: new BorderRadius.all(const Radius.circular(5.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors /*.black,*/.grey[600],
                    blurRadius: 6.0,
                    // has the effect of softening the shadow
                    //spreadRadius: 1.0, // has the effect of extending the shadow
                    offset: Offset(0.0, 2.0),
                  )
                ]),
            child: new Text(
              _clientState.type == "Buscar" ? "OK" : _clientState.type,
              style: new TextStyle(
                color: Colors.white,
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.3,
              ),
            ),
          )),
    );
  }

  void managerClient(){

    ClienteState._allow = false;
    switch(_clientState.type){

      case "Excluir":

        _clientState.clienteController.removeClient(_clientState._cLienteModel.getId());
        break;

      case "Alterar":

        _clientState.clienteController.updateClient(_clientState.nameController.text, _clientState.cpfController.text,
            _clientState.identidadeController.text, _clientState.allowPicture, _clientState.emailController.text,
            _clientState.addressController.text);
        break;

      case "Incluir":

        _clientState.clienteController.createClient(
            _clientState.nameController.text,
            _clientState.cpfController.text,
            _clientState.identidadeController.text,
            _clientState.allowPicture, _clientState.emailController.text,
            _clientState.addressController.text);
        break;

      case "Buscar":

        backHome();
        break;
    }
  }
  
  backHome(){

    int time = _clientState.type == "Buscar" ? 200 : 1000;
    Future.delayed(Duration(milliseconds: time), () {
      Navigator.pop(MyApp.genericContext);
    });
  }
}