import 'dart:async';
import 'package:doggis/controllers/LoginController.dart';
import 'package:doggis/utils/keys_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashScreen extends StatefulWidget{

  @override
  _SplashScreen createState() => new _SplashScreen();
}

class _SplashScreen extends State<SplashScreen> with TickerProviderStateMixin{

  AnimationController controller;
  Animation<double> animation;
  bool _visible = false;

  @override
  Widget build(BuildContext context) {

    SystemChrome.setEnabledSystemUIOverlays([]);

    return new Scaffold(
        key: Key(splashScreenKey),
        backgroundColor: Colors.yellow[300],
        body: new Center(
          child: Container(
            child: AnimatedOpacity(
              opacity: _visible ? 1.0 : 0.0,
              duration: Duration(milliseconds: 1500),
              child: new Image.asset("assets/images/logo.png", width: 60, height: 60),
            ),
          ),
        )
    );
  }

  @override
  void initState() {

    super.initState();
    startAnimationTimer();
  }

  startAnimationTimer() async{

    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, _animationFade);
  }

  startSplashScreenTimer() async{

    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, _navigationNextPage);
  }

  _animationFade(){

    setState(() {
      _visible = true;
    });
    startSplashScreenTimer();
  }

  _navigationNextPage(){

    LoginController().signOut();
    Navigator.of(context).pushReplacementNamed('/Login');
  }
}