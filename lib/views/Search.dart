import 'package:doggis/controllers/SearchController.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:doggis/utils/keys_constants.dart';
import 'package:doggis/views/Client.dart';
import 'package:doggis/views/Generic.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Agendamento.dart';
import 'Home.dart';

class BuscarClientes extends StatefulWidget {

  BuildContext contextEvent;
  static bool home;
  BuscarClientes({ Key key, @required this.contextEvent}) : super(key: key);
  @override
  BuscarClientesState createState() => new BuscarClientesState(this.contextEvent);
}

class BuscarClientesState extends State<BuscarClientes> with Generic{

  BuildContext _contextEvent;
  Widget appBarTitle = new Text("Consulta Cliente", style: new TextStyle(color: Colors.white));
  Icon actionIcon = new Icon(Icons.search, color: Colors.white,);
  final TextEditingController _searchQuery = new TextEditingController();
  List<ClienteModel> _list;
  List<ChildItem> listChild = new List();
  bool _IsSearching;
  static String searchText = "";
  SearchController _searchController;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  BuscarClientesState(this._contextEvent) {

    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _IsSearching = false;
          searchText = "";
          listChild.clear();
        });
      }
      else {
        setState(() {
          _IsSearching = true;
          searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {

    super.initState();
    _IsSearching = false;
  }

   Widget _load(List<ClienteModel> listClientes){

    _list = listClientes;
    for(var clienteModel in _list){
      listChild.add(new ChildItem(_contextEvent, clienteModel, _searchController));
    }

    return CriarList(this, _contextEvent, listChild, listClientes, _searchController, _IsSearching);
  }

  Widget _showProgressBar(SearchController searchController){

    _searchController = searchController;
    searchController.getAllClientes();
    return Center(child: CircularProgressIndicator());
  }

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
        builder: (context) => SearchController(),
        child: new Scaffold(
            key: _scaffoldKey,
            resizeToAvoidBottomInset: false,
            appBar: buildBar(context),
            body: Consumer<SearchController>(
                builder: (context, searchController, _) =>
                searchController.listClientes == null ? _showProgressBar(
                    searchController) :
                _load(searchController.listClientes)))
    );
  }

  Widget buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: appBarTitle,
      actions: <Widget>[
        new IconButton(
          icon: actionIcon,
          onPressed: () {
            _displayTextField();
          },
        ),
      ],
    );
  }

  void _displayTextField() {
    setState(() {
      if (this.actionIcon.icon == Icons.search) {
        this.actionIcon = new Icon(
          Icons.close,
          color: Colors.white,
        );
        this.appBarTitle = new TextField(
          autofocus: true,
          controller: _searchQuery,
          style: new TextStyle(
            color: Colors.white,
          ),
        );

        _handleSearchStart();
      } else {
        _handleSearchEnd();
      }
    });
  }

  void _handleSearchStart() {
    setState(() {
      _IsSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(Icons.search, color: Colors.white,);
      this.appBarTitle =
      new Text("Consultar Cliente", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold),);
      _IsSearching = false;
      _searchQuery.clear();
      listChild.clear();
    });
  }

}

class CriarList extends StatelessWidget {

  List<ChildItem> listChild;
  List<ClienteModel> list;
  SearchController searchController;
  bool IsSearching;
  BuildContext contextReturn;
  BuscarClientesState searchListState;

  CriarList(this.searchListState, BuildContext contextReturn, List<ChildItem> listChild, List<ClienteModel> list, SearchController searchController, bool IsSearching){

    this.contextReturn = contextReturn;
    this.listChild = listChild;
    this.list = list;
    this.searchController = searchController;
    this.IsSearching = IsSearching;
  }
  @override
  Widget build(BuildContext context) {

    final cliente = Provider.of<SearchController>(context).clienteCarregado;
    if(cliente != null) {


      Future.delayed(Duration(milliseconds: 1800), () {
        (cliente) ?  Navigator.pop(contextReturn) : searchListState.snackBar(searchListState._scaffoldKey, "O cliente não possui pet cadastrado!");
      });

//      (cliente) ?  Navigator.push(context, MaterialPageRoute(builder: (context) => Agendamento(selectedDay: HomeState.selectedDay, listScheduleModel: null)))
//          : searchListState.snackBar(searchListState._scaffoldKey, "O cliente não possui pet cadastrado!");

    }
    return Container(
      color: Colors.yellow[300],
      child: ListView(
      key: Key(listViewKey),
      padding: new EdgeInsets.symmetric(vertical: 8.0),
      children: IsSearching ? _buildSearchList() : _buildList(),
    ));
  }

  List<ChildItem> _buildList() {
    return listChild;
  }

  List<ChildItem> _buildSearchList() {
    if (BuscarClientesState.searchText.isEmpty) {
      return listChild;
    }
    else {
      List<ChildItem> _searchList = List();
      for (int i = 0; i < list.length; i++) {
        ClienteModel clienteModel = list[i];
        if (clienteModel.getNome().toLowerCase().contains(BuscarClientesState.searchText.toLowerCase())) {
          _searchList.add(new ChildItem(contextReturn, clienteModel, searchController));
        }
      }
      return _searchList;
    }
   }
  }

class ChildItem extends StatelessWidget {

  final ClienteModel _clienteModel;
  SearchController _searchController;
  BuildContext contextReturn;

  ChildItem(this.contextReturn, this._clienteModel, this._searchController);

  @override
  Widget build(BuildContext context) {

    return ListTile(title: new Text(_clienteModel.getNome()), onTap: (){

      if(BuscarClientes.home != null && BuscarClientes.home){
       // _searchController.processFinish(true);
        MyApp.preLoadModel.setClienteModel(_clienteModel);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Client(Generic.type)));
      }else{
        _searchController.getPetsByClientes(_clienteModel);
      }

    });
  }
}

