import 'package:doggis/controllers/CalendarController.dart';
import 'package:doggis/dao/impl/FuncionarioDaoImpl.dart';
import 'package:doggis/dao/impl/ServicosDaoImpl.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/AtendenteModel.dart';
import 'package:doggis/models/ServicoModel.dart';
import 'package:doggis/utils/keys_constants.dart';
import 'package:doggis/views/components/calendar/customization/calendar_style.dart';
import 'package:doggis/views/components/calendar/customization/header_style.dart';
import 'package:doggis/views/components/calendar/table_calendar.dart';
import 'package:doggis/views/Agendamento.dart';
import 'package:doggis/views/Generic.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';


class Home extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new HomeState();
  }
}

class HomeState extends State<Home> with Generic, TickerProviderStateMixin {

  static DateTime selectedDay;
  Map<DateTime, List> events;
  List selectedEvents;
  Map<DateTime, List> _visibleEvents;
  AnimationController _controller;
  CalendarController calendarController;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  

  getList(){

    if(calendarController != null){
       calendarController.getAgenda(selectedDay);
    }
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance
        .addPostFrameCallback((_) => getList());

    selectedDay = DateTime.now();
    events = {};
    selectedEvents = events[selectedDay] ?? [];
    _visibleEvents = events;

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _controller.forward();
    cargaPreliminar();
  }

  void cargaPreliminar(){

    ServicoDaoImpl(null).getTodosServicos();
    FuncionarioDaoImpl(null).getFuncionarios();
  }

  @override
  Widget build(BuildContext context) {

    MyApp.genericContext = context;
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.top, SystemUiOverlay.bottom]);
    return ChangeNotifierProvider(
      builder: (context) => calendarController = CalendarController(),
      child: Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: commonAppBar("Teste"),
      body: Container(
        color: Colors.yellow[300],
        width: double.maxFinite,
        height: double.maxFinite,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[

            Consumer<CalendarController>(builder: (context, searchController, _) => _buildTableCalendar()),
            Consumer<CalendarController>(builder: (context, searchController, _) => loadContainer()),
            Consumer<CalendarController>(builder: (context, searchController, _) =>
            searchController.listServicos != null ? Expanded(child: _buildEventList(searchController.listServicos)) :
            Expanded(
                child: Container(
                    color: Colors.brown[300],
                    width: double.infinity,
                    child: Center(
                        child: searchController.showProgress ? CircularProgressIndicator(backgroundColor: Colors.white, valueColor: new AlwaysStoppedAnimation<Color>(Colors.brown)) : Text("Adicione Eventos ", style: TextStyle(fontSize: 30, color: Colors.white))
                    ))))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        key: Key(floatingActionButtonKey),
        onPressed: () {

          if(MyApp.preLoadModel.getUser() is AtendenteModel) {
            goEvent();
          }else{
            snackBar(_scaffoldKey, "Você não possui privilégios para criar agendamentos!");
          }
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.brown,
      ),
    ));
  }

  Widget loadContainer() {
    return selectedEvents != null && selectedEvents.isEmpty ? SizedBox(height: 4.0) : Container(
        width: double.infinity,
        height: 25,
        color: Colors.brown,
        child: Center(
            child: Text("EVENTOS DO DIA " + selectedDay.day.toString(),
                style: TextStyle(fontWeight: FontWeight.bold,
                    fontSize: 12,
                    color: Colors.white))
        )
    );
  }

  goEvent() async {

    var result = await  Navigator.push(context, MaterialPageRoute(builder: (context) => Agendamento(selectedDay: HomeState.selectedDay, listScheduleModel: calendarController.listAgendaByDay,)));
    if(result == null) {
      print("retornou");
      if (Generic.updateEvents != null && Generic.updateEvents) {
        Generic.updateEvents = false;
        calendarController.getAgenda(selectedDay);
      }
    }
  }

  Widget _buildTableCalendar() {
    return TableCalendar(
      events: _visibleEvents,
      initialCalendarFormat: CalendarFormat.week,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      startDay: getYesterday(),
      calendarStyle: CalendarStyle(
        selectedColor: Colors.brown,
        todayColor: Colors.brown[200],
        markersColor: Colors.brown[700],
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle: TextStyle().copyWith(
            color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Colors.brown[400],
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }

  DateTime getYesterday() {

    DateTime now = new DateTime.now();
    return new DateTime(now.year, now.month, now.day - 1, 24);
  }

  void _onDaySelected(DateTime day, List events) {

    setState(() {
      selectedDay = day;
      calendarController.getAgenda(selectedDay);
      selectedEvents = events;
      calendarController.showProgress = true;
    });
  }

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    setState(() {
      _visibleEvents = Map.fromEntries(
        events.entries.where(
              (entry) =>
          entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );
    });
  }

  Widget _buildEventList(Map<DateTime, List> listServicos) {

    List selectedEvents = listServicos[HomeState.selectedDay];
    if (selectedEvents != null) {

      this.selectedEvents = selectedEvents;
      _visibleEvents = listServicos;
      return ListView(
        children: selectedEvents
            .map((event) =>
            Container(
              decoration: BoxDecoration(
                border: Border.all(width: 0.8),
                borderRadius: BorderRadius.circular(12.0),
              ),
              margin: const EdgeInsets.symmetric(
                  horizontal: 8.0, vertical: 4.0),
              child: ListTile(
                title: Text((event as ServicoModel).getTipoServico()),
                onTap: () => snackBar(_scaffoldKey, "Item escolhido: " + (event as ServicoModel).getTipoServico()),
              ),
            ))
            .toList(),
      );
    }else{
      return SizedBox();
    }
  }
}





