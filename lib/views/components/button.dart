import 'package:flutter/material.dart';

class ButtonApp extends StatelessWidget{

  static const String TYPE_COMMON_BUTTON= "commonButton";
  String type;
  String text;

  ButtonApp({Key key, @required this.type, @required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _managerText();
  }

  Widget _managerText(){

    switch(type){

      case TYPE_COMMON_BUTTON:
        return _button();
    }
  }

  Widget _button(){

    return new Container(
      width: 320.0,
      height: 55.0,
      alignment: FractionalOffset.center,
      decoration: new BoxDecoration(
        color: Colors.brown,
        borderRadius: new BorderRadius.all(const Radius.circular(25.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors /*.black,*/ .brown[500],
              blurRadius: 4.0,
              // has the effect of softening the shadow
              //spreadRadius: 1.0, // has the effect of extending the shadow
              offset: Offset(0.0, 1.0),
            )
          ]),
      child: new Text(
        text,
        style: new TextStyle(
          color: Colors.white,
          fontSize: 15.0,
          fontWeight: FontWeight.bold,
          letterSpacing: 0.3,
        ),
      ),
    );
  }
}