import 'package:flutter/material.dart';

class TextFieldApp extends StatelessWidget{

  static const String TYPE_TXT_FIELD_INPUT_DECORATION = "txtFieldInputDecoration";
  ThemeData theme;
  String type;
  String text;
  FocusNode fieldFocusId ;
  FocusNode nextFieldFocusId ;
  BuildContext parentContext;
  Function callbackFunction;
  Function saveFieldCallbackFunction; //save function to be called on the parent widget

  TextFieldApp({Key key,
    @required this.type,
    @required this.text,
    this.fieldFocusId,
    this.nextFieldFocusId ,
    this.parentContext,
    this.callbackFunction,
    this.saveFieldCallbackFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    theme = Theme.of(context);
    return _managerTextField();
  }

  Widget _managerTextField(){

    switch(type){

      case TYPE_TXT_FIELD_INPUT_DECORATION:
        return _txtFieldInputDecotation();
    }
  }

  Widget _txtFieldInputDecotation(){

    return new Theme(
      data: theme.copyWith(primaryColor: Colors.brown),
      child: new TextFormField(
        validator: (value) {
          if (value.length == 0 ) {
            return ('informe os dados.');
          }
        },
        style: TextStyle(color: Colors.brown, fontSize: 20),
        textInputAction: nextFieldFocusId != null ? TextInputAction.next : TextInputAction.done,
        focusNode: fieldFocusId,
        obscureText: nextFieldFocusId != null ? false : true,
        maxLength: nextFieldFocusId != null ? 40 : 8,
        onFieldSubmitted:(String inputText){
          if(nextFieldFocusId != null)
            FocusScope.of(parentContext).requestFocus(nextFieldFocusId);
          else
            callbackFunction();
        },
        onSaved: (String textInput) {
          // called when the form calls save on the textFormFields
          saveFieldCallbackFunction(textInput);
        },
        decoration: new InputDecoration(
          labelText: text != null ? text : ""),
        ),
    );
  }
}