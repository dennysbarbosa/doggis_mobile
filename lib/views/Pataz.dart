import 'package:doggis/controllers/ClienteController.dart';
import 'package:doggis/views/Generic.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Pataz extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return new PatazState();
  }
}

class PatazState extends State<Pataz> with Generic{

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
      builder: (context) => ClienteController(),
      child: Scaffold(
      backgroundColor: Colors.white,
      appBar: commonAppBar("Meus Pataz"),
      //appBar: ("Meus Pataz", true),
      body: Container(
        margin: EdgeInsets.only(left: 10, right: 10),
        child: Stack(
            children: <Widget>[
              Align(
                child: Image.asset("assets/images/pata2.png"),
              ),
              Align(
                  child: Container(
                   margin: EdgeInsets.only(top: 80),
                   child: QtdPatazWidget())
                  )
            ]
        ),
      ),
      )
    );
  }
}

class QtdPatazWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    final cliente = Provider.of<ClienteController>(context).clienteModel;
    return cliente == null ?  CircularProgressIndicator(backgroundColor: Colors.white, valueColor: new AlwaysStoppedAnimation<Color>(Colors.black)) : Text(cliente.getQtdPataz().toString(), style: TextStyle(fontSize: 50, color: Colors.white));
  }
}