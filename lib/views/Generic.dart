
import 'dart:async';

import 'package:doggis/main.dart';
import 'package:doggis/models/AtendenteModel.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:doggis/views/Client.dart';
import 'package:doggis/views/Search.dart';
import 'package:flutter/material.dart';
import 'package:doggis/utils/keys_constants.dart';

abstract class Generic{

  static bool updateEvents;
  List<String> list = new List();
  static String type;

  final List<String> _dropdownValues = [
    "Gerenciar cliente:",
    "Incluir",
    "Alterar",
    "Excluir",
    "Buscar"
  ];

  Widget dropdownWidget() {

    return new Theme(
        data: Theme.of(MyApp.genericContext).copyWith(
            canvasColor: Theme.of(MyApp.genericContext).primaryColor
        ),
    child: DropdownButtonHideUnderline(
      child: DropdownButton(key: Key(dropDownButtonKey),
      items: _dropdownValues
          .map((value) => DropdownMenuItem(
        child: Text(value, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13, color: Colors.white),),
        value: value,
      ))
          .toList(),
        onChanged: goPage,
      isExpanded: false,
      value: _dropdownValues.first,
    )
    ));
  }

  goPage(String value){

    Generic.type = value;
    switch(value){
      case "Incluir":

        Navigator.push(MyApp.genericContext, MaterialPageRoute(builder: (context) => Client("Incluir")));
        break;

      case "Alterar":
      case "Excluir":
      case "Buscar":

        goList();
        break;

    }
  }

  void goList() async {

    BuscarClientes.home = true;
    var result = await Navigator.of(MyApp.genericContext).push(MaterialPageRoute(
        builder: (context) =>
            BuscarClientes(
              contextEvent: MyApp.genericContext,
            )));
    if (result == null) {
      print("retornou");
      BuscarClientes.home = false;
    }
  }

  PreferredSize commonAppBar(String text) {

    return PreferredSize(
     preferredSize: Size.fromHeight(66.0),
      child: AppBar(
        title:Padding(
          padding: const EdgeInsets.only(top: 5),
        child: new Image.asset(
            'assets/images/logo.png', width: 45, height: 45),
        ),
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Padding(
            padding: const EdgeInsets.only(top: 40, left: 70),
            child: Row(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text("Olá, " + MyApp.preLoadModel.getUser().getNome(), style: TextStyle(
                        fontFamily: "fonts/dosis-bold.ttf",
                        fontSize: 12)),

                    Text(configTextTitle(), style: TextStyle(
                        fontFamily: "fonts/dosis-bold.ttf",
                        fontSize: 12))

                  ],
                ),
              ],
            ),
          ),
        ),
      actions: <Widget>[
        MyApp.preLoadModel.getUser() is AtendenteModel ? dropdownWidget() : Container(),
      ],
     ),
    );
  }

  String configTextTitle(){

    if(MyApp.preLoadModel.getUser() is ClienteModel){
      return "Seus Pataz";
    }else if(MyApp.preLoadModel.getUser() is AtendenteModel){
      return "Atendente";
    }else {
      return "Veterinário";
    }
  }

  snackBar(GlobalKey<ScaffoldState> scaffoldKey, String value){

    Future.delayed(Duration(milliseconds: 200)).then((e) {
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
    });
  }

  static Future<void> ackAlert() {
    return showDialog<void>(
      context: MyApp.genericContext,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Ops,'),
          content: const Text('Verifique sua conexão com a internet!'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}