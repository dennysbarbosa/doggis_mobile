import 'package:doggis/models/UsuarioModel.dart';
import 'package:doggis/models/PetModel.dart';

class ClienteModel extends UsuarioModel{

  String endereco;
  String email;
  int qtdPataz;
  bool permitirFoto;
  List<PetModel> listPets;

  setEndereco(String endereco){
    this.endereco = endereco;
  }

  getEndereco() {
    return this.endereco;
  }

  setEmail(String email){
    this.email = email;
  }

  getEmail(){
    return this.email;
  }

  setQtdPataz(int qtdPataz){
    this.qtdPataz = qtdPataz;
  }

  getQtdPataz(){
    return this.qtdPataz;
  }

  setPermitirFoto(bool permitirFoto){
    this.permitirFoto = permitirFoto;
  }

  getPermitirFoto(){
    return this.permitirFoto;
  }


  setListPets(List<PetModel> listPets){
    this.listPets = listPets;
  }

  getListPets(){
    return this.listPets;
  }

}