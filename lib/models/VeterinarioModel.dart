import 'package:doggis/models/UsuarioModel.dart';

class VeterinarioModel extends UsuarioModel {

  String registroCV;
  List<String> listTipos;
  bool disponivel;

  setRegistroCV(String registroCV){
    this.registroCV = registroCV;
  }

  getRegistroCV(){
    return this.registroCV;
  }

  setList(List<String> listTipos){
    this.listTipos = listTipos;
  }

  getListTipos(){
    return this.listTipos;
  }

  setDisponivel(bool disponivel){
    this.disponivel = disponivel;
  }

  getDisponivel(){
    return this.disponivel;
  }

}

