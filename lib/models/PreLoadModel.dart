import 'package:doggis/models/ServicoModel.dart';
import 'package:doggis/models/ClienteModel.dart';
import 'package:doggis/models/UsuarioModel.dart';
import 'package:firebase_auth/firebase_auth.dart';

class PreLoadModel{

  UsuarioModel userModel;
  List<UsuarioModel> _listFunc;
  List<ClienteModel> _listClientes;
  List<ServicoModel> listServicos;
  ClienteModel clienteModel;


  setUser(UsuarioModel userModel){
    this.userModel = userModel;
  }

  getUser(){
    return this.userModel;
  }

  setListFunc(List<UsuarioModel> listFunc){
    this._listFunc = listFunc;
  }

  getListFunc(){
    return this._listFunc;
  }

  setListClientes(List<ClienteModel> listClientes){
    this._listClientes = listClientes;
  }

  getListClientes(){
    return this._listClientes;
  }
  setListServicos(List<ServicoModel> listServicos){
    this.listServicos = listServicos;
  }

  getListServicos(){
    return this.listServicos;
  }

  setClienteModel(ClienteModel clienteModel){
    this.clienteModel = clienteModel;
  }

  getClienteModel(){
    return this.clienteModel;
  }
}

