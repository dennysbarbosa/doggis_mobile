
class AgendaModel{

  String id;
  String clienteId;
  String petId;
  String tipoServicoId;
  String profissionalId;
  String data;
  String horario;

  setId(String id){
    this.id = id;
  }

  String getId(){
    return this.id;
  }

  setClienteID(String clienteID){
    this.clienteId = clienteID;
  }

  getClienteID(){
    return this.clienteId;
  }

  setPetID(String petID){
    this.petId = petID;
  }

  getPetID(){
    return this.petId;
  }

  setTipoServicoId(String tipoServicoId){
    this.tipoServicoId = tipoServicoId;
  }

  getTipoServicoId(){
    return this.tipoServicoId;
  }

  setProfissionalId(String profissionalId){
    this.profissionalId = profissionalId;
  }

  getProfissionalId(){
    return this.profissionalId;
  }

  setData(String data){
    this.data = data;
  }

  getData(){
    return this.data;
  }

  setHorario(String horario){
    this.horario = horario;
  }

  getHorario(){
    return this.horario;
  }
}