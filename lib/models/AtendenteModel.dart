import 'package:doggis/models/UsuarioModel.dart';

class AtendenteModel extends UsuarioModel {

  String _cargo;
  bool _disponivel;

  setCargo(String cargo){
    this._cargo = cargo;
  }

  String getCargo(){
    return this._cargo;
  }

  setDisponivel(bool disponivel){
    this._disponivel = disponivel;
  }

  getDisponivel(){
    return this._disponivel;
  }
}