
class PetModel{

  String id;
  String nome;
  String raca;
  String porte;
  String alergia;
  String descricao;
  String clienteID;
  String tipo;

  setId(String id){
    this.id = id;
  }

  getId(){
    return this.id;
  }

  setNome(String nome){
    this.nome = nome;
  }

  getNome(){
    return this.nome;
  }

  setRaca(String raca){
    this.raca = raca;
  }

  getRaca(){
    return this.raca;
  }

  setPorte(String porte){
    this.porte = porte;
  }

  getPorte(){
    return this.porte;
  }

  setAlergia(String alergia){
    this.alergia = alergia;
  }

  getAlergia(){
    return this.alergia;
  }

  setDescricao(String descricao){
    this.descricao = descricao;
  }

  setClienteID(String clienteID){
    this.clienteID = clienteID;
  }

  getClienteID(){
    return this.clienteID;
  }

  setTipo(String tipo){
    this.tipo = tipo;
  }

  getTipo(){
    return this.tipo;
  }
}