
import 'package:doggis/controllers/AgendamentoController.dart';
import 'package:doggis/dao/impl/ClienteDaoImpl.dart';
import 'package:doggis/dao/impl/FuncionarioDaoImpl.dart';
import 'package:doggis/main.dart';
import 'package:doggis/models/AtendenteModel.dart';
import 'package:doggis/models/VeterinarioModel.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:doggis/utils/field_validators.dart';

void main() {

  test('Empty Email and Password Test', () {
    var result = LoginFieldValidator.validate(null, null);
    expect(result, 'Campos, login e senha devem ser preenchidos.');
  });

  test('ScheduleController type employee Atendente', (){

    AgendamentoController scheduleController = AgendamentoController();
    AtendenteModel atendenteModel = new AtendenteModel();
    atendenteModel.setId("1234567");
    atendenteModel.setNome('usuario teste');
    scheduleController.usuarioModel = atendenteModel;
    scheduleController.validarTypeFunc();
    expect(scheduleController.typeFunc, 2);
  });

  test('ScheduleController type employee Veterinario', (){

    AgendamentoController scheduleController = AgendamentoController();
    VeterinarioModel veterinarioModel = new VeterinarioModel();
    veterinarioModel.setId("1234567");
    veterinarioModel.setNome('usuario teste');
    scheduleController.usuarioModel = veterinarioModel;
    scheduleController.validarTypeFunc();
    expect(scheduleController.typeFunc, 2);
  });

  test('LoginController with Firebase Employee', (){
    FuncionarioDaoImpl(null).getFuncByID('kT15Lmjq70WWMNzejN3zcJtdiHe2');
    Future.delayed(const Duration(milliseconds: 2000), (){
      expect(MyApp.preLoadModel.userModel, MyApp.preLoadModel.userModel != null);
    });
  });

  test('LoginController with Firebase Client', (){
    ClienteDaoImpl(null).getClienteById('pBudirfwgLNiKmkuLH5KA7yYEBS2');
    Future.delayed(const Duration(milliseconds: 2000), (){
      expect(MyApp.preLoadModel.userModel, MyApp.preLoadModel.userModel != null);
    });
  });

}
