// Imports the Flutter Driver API
import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
import 'finders.dart';

void main() {
  // starts driver
  FlutterDriver driver;
  // Connect to the Flutter driver before running any tests
  setUpAll(
        () async {
      driver = await FlutterDriver.connect();
    },
  );

  // Close the connection to the driver after the tests have completed
  tearDownAll(
        () async {
      if (driver != null) {
        driver.close();
      }
    },
  );

  group(
    'Login Screen Tests',
        () {

      String _login = 'clarinhagitahy@doggis.com.br';
      String _pwd = 'teste123';
      void checkLoginScreenWidgetsExistance() async {
        await driver.waitFor(loginScreenFieldFinder);
        await driver.waitFor(loginScreenPwdFieldFinder);
      }



      test(
        'check flutter driver health',
            () async {
          Health health = await driver.checkHealth();
          print(health.status);
        },
      );
      test(
        'Wait splash screen go away',
            () async {
          await driver.waitUntilNoTransientCallbacks();
          await driver.waitForAbsent(splashScreenFinder);
        },
      );
      test(
        'Check Login Screen Widgets Existance',
            () async {
          checkLoginScreenWidgetsExistance();
        },
      );

      test(
        'Enter Empty Login/Password = FAIL',
            () async {
          await driver.tap(loginScreenFieldFinder);
          await driver.enterText('');
          await driver.tap(loginScreenPwdFieldFinder);
          await driver.enterText('');
          await driver.tap(loginScreenEnterButtonFinder);
          await driver.waitForAbsent(
              find.byTooltip('Campos, login e senha devem ser preenchidos.'));
        },
      );

      test(
        'Enter text and Validate Login widh Atendente = OK',
            () async {
          //enters login
          await driver.tap(loginScreenFieldFinder);
          await driver.enterText(_login);
          //enters pwd
          await driver.tap(loginScreenPwdFieldFinder);
          await driver.enterText(_pwd);

          //tap validate button
          sleep(Duration(seconds: 2));
          await driver.tap(loginScreenEnterButtonFinder);
           //await for homeScreen = login successful -> finds first navy bar button
          await driver.waitFor(floatingActionButtonFinder);
        },
      );
    }, // end group body def
  );

  group('Home Screen Tests', () {
    test(
        'Go Schedule', () async{
       // await driver.tap(dropDownButtonFinder);
        sleep(const Duration(milliseconds: 2000));
       // await driver.tap(dropDownButtonFinder);
        await driver.tap(floatingActionButtonFinder);

        //await driver.tap(find.byTooltip('Back'));
    });

    test('Schedule Screen Tests', () async {
      sleep(Duration(seconds: 2));
      await driver.waitFor(iconButtonFinder);
      await driver.tap(iconButtonFinder);
      await driver.waitFor(listViewFinder);
      sleep(const Duration(milliseconds: 2000));
      await driver.tap(find.byTooltip('Back'));
      sleep(const Duration(milliseconds: 2000));
      await driver.tap(find.byTooltip('Back'));
      sleep(const Duration(milliseconds: 2000));
      await driver.tap(dropDownButtonFinder);
      sleep(const Duration(milliseconds: 2000));
      await driver.tap(find.text('Incluir'));
      sleep(const Duration(milliseconds: 2000));
    });
  });
}



