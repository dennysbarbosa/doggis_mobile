import 'package:flutter_driver/driver_extension.dart';
import '../lib/main.dart' as testApp ;

// Run on terminal :
// flutter drive --target=test_driver/app.dart
void main(){
  // enables flutter driver
  enableFlutterDriverExtension();
  testApp.main() ;
}