import 'package:doggis/utils/keys_constants.dart';
import 'package:flutter_driver/flutter_driver.dart';



//Splash
final splashScreenFinder = find.byValueKey(splashScreenKey);

/// >>>>>> Login Screen Finders <<<<<<
final loginScreenFieldFinder = find.byValueKey(loginScreenLoginFieldKey);
final loginScreenPwdFieldFinder = find.byValueKey(loginScreenPwdFieldKey);
final loginScreenEnterButtonFinder = find.byValueKey(loginScreenEnterButtonKey);

/// >>>>>> Home Screen <<<<<<<
final floatingActionButtonFinder = find.byValueKey(floatingActionButtonKey);
final dropDownButtonFinder = find.byValueKey(dropDownButtonKey);
//final loginScreenForgotPwdButtonFinder = find.byValueKey(loginScreenForgotPwdButtonKey);
//final versionCheckDialogFinder = find.byValueKey(versionCheckDialogKey);
//final versionCheckDialogEnterKeyFinder = find.byValueKey(versionCheckDialogEnterKey);
//final versionCheckDialogCancelKeyFinder = find.byValueKey(versionCheckDialogCancelKey);

final radioButtonFinder = find.byValueKey(radioButtonKey);
final iconButtonFinder = find.byValueKey(iconButtonKey);

final listViewFinder = find.byValueKey(listViewKey);
